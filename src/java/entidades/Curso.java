/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Johan Leonardo
 */
@Entity
@Table(name = "curso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Curso.findAll", query = "SELECT c FROM Curso c")
    , @NamedQuery(name = "Curso.findByIdCurso", query = "SELECT c FROM Curso c WHERE c.idCurso = :idCurso")
    , @NamedQuery(name = "Curso.findByNombres", query = "SELECT c FROM Curso c WHERE c.nombres = :nombres")
    , @NamedQuery(name = "Curso.findByDescripcion", query = "SELECT c FROM Curso c WHERE c.descripcion = :descripcion")
    , @NamedQuery(name = "Curso.findByPrecio", query = "SELECT c FROM Curso c WHERE c.precio = :precio")
    , @NamedQuery(name = "Curso.findByStock", query = "SELECT c FROM Curso c WHERE c.stock = :stock")
    , @NamedQuery(name = "Curso.findByTiempoDeDuracion", query = "SELECT c FROM Curso c WHERE c.tiempoDeDuracion = :tiempoDeDuracion")
    , @NamedQuery(name = "Curso.findByNombredelprofesor", query = "SELECT c FROM Curso c WHERE c.nombredelprofesor = :nombredelprofesor")
    , @NamedQuery(name = "Curso.findByRequisitos", query = "SELECT c FROM Curso c WHERE c.requisitos = :requisitos")
    , @NamedQuery(name = "Curso.findByFechaderegistro", query = "SELECT c FROM Curso c WHERE c.fechaderegistro = :fechaderegistro")})
public class Curso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCurso")
    private Integer idCurso;
    @Size(max = 200)
    @Column(name = "Nombres")
    private String nombres;
    @Lob
    @Column(name = "Foto")
    private byte[] foto;
    @Size(max = 255)
    @Column(name = "Descripcion")
    private String descripcion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Precio")
    private Float precio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Stock")
    private int stock;
    @Column(name = "tiempo_de_duracion")
    private Float tiempoDeDuracion;
    @Size(max = 40)
    @Column(name = "nombredelprofesor")
    private String nombredelprofesor;
    @Size(max = 200)
    @Column(name = "Requisitos")
    private String requisitos;
    @Column(name = "Fecha_de_registro")
    @Temporal(TemporalType.DATE)
    private Date fechaderegistro;

    public Curso() {
    }

    public Curso(Integer idCurso) {
        this.idCurso = idCurso;
    }

    public Curso(Integer idCurso, int stock) {
        this.idCurso = idCurso;
        this.stock = stock;
    }

    public Integer getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Integer idCurso) {
        this.idCurso = idCurso;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Float getTiempoDeDuracion() {
        return tiempoDeDuracion;
    }

    public void setTiempoDeDuracion(Float tiempoDeDuracion) {
        this.tiempoDeDuracion = tiempoDeDuracion;
    }

    public String getNombredelprofesor() {
        return nombredelprofesor;
    }

    public void setNombredelprofesor(String nombredelprofesor) {
        this.nombredelprofesor = nombredelprofesor;
    }

    public String getRequisitos() {
        return requisitos;
    }

    public void setRequisitos(String requisitos) {
        this.requisitos = requisitos;
    }

    public Date getFechaderegistro() {
        return fechaderegistro;
    }

    public void setFechaderegistro(Date fechaderegistro) {
        this.fechaderegistro = fechaderegistro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCurso != null ? idCurso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Curso)) {
            return false;
        }
        Curso other = (Curso) object;
        if ((this.idCurso == null && other.idCurso != null) || (this.idCurso != null && !this.idCurso.equals(other.idCurso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Curso[ idCurso=" + idCurso + " ]";
    }
    
}
